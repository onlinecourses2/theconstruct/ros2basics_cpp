#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from ament_index_python.packages import get_package_prefix


def generate_launch_description():

    pkg_gazebo_ros = get_package_share_directory('gazebo_ros')

    animated_actors_package = "animated_actors"
    animated_actors_package_path = get_package_share_directory(
        animated_actors_package)
    animated_actors_path = os.path.join(animated_actors_package_path, 'models')

    animated_actors_lib_path = get_package_prefix(
        animated_actors_package)

    # Gazebo Plugins
    gazebo_plugins_name = "gazebo_plugins"
    gazebo_plugins_name_path_install_dir = get_package_prefix(
        gazebo_plugins_name)

    if 'GAZEBO_MODEL_PATH' in os.environ:
        os.environ['GAZEBO_MODEL_PATH'] = os.environ['GAZEBO_MODEL_PATH'] + \
            ':' + animated_actors_path
    else:
        os.environ['GAZEBO_MODEL_PATH'] = animated_actors_path

    if 'GAZEBO_PLUGIN_PATH' in os.environ:
        os.environ['GAZEBO_PLUGIN_PATH'] = os.environ['GAZEBO_PLUGIN_PATH'] + ':' + \
            gazebo_plugins_name_path_install_dir + '/lib' + \
            ':' + animated_actors_lib_path + '/lib/' + animated_actors_package
    else:
        os.environ['GAZEBO_PLUGIN_PATH'] = gazebo_plugins_name_path_install_dir + \
            '/lib' + ':' + animated_actors_lib_path + '/lib/' + animated_actors_package

    print("GAZEBO MODELS PATH=="+str(os.environ["GAZEBO_MODEL_PATH"]))
    print("GAZEBO PLUGINS PATH=="+str(os.environ["GAZEBO_PLUGIN_PATH"]))

    # Gazebo launch
    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_gazebo_ros, 'launch', 'gazebo.launch.py'),
        )
    )

    return LaunchDescription([
        DeclareLaunchArgument(
            'world',
            default_value=[os.path.join(
                animated_actors_package_path, 'worlds', 'actor_space.world'), ''],
            description='SDF world file'),
        DeclareLaunchArgument(
            'verbose', default_value='true',
            description='Set "true" to increase messages written to the terminal.'
        ),
        gazebo
    ])
