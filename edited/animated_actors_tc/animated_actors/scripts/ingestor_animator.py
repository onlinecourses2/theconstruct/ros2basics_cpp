#! /usr/bin/env python3

import rclpy
# import the ROS2 python libraries
from rclpy.node import Node
from rclpy.qos import ReliabilityPolicy, QoSProfile

from rmf_ingestor_msgs.msg import IngestorState

import time

from std_msgs.msg import String

from rclpy.callback_groups import MutuallyExclusiveCallbackGroup
from rclpy.executors import MultiThreadedExecutor

from std_msgs.msg import String


class AnimatedIngestor(Node):

    def __init__(self, ingestor_name="table_ingestor_1"):
        super().__init__('manual_dispenser')

        self.group1 = MutuallyExclusiveCallbackGroup()
        self.group3 = MutuallyExclusiveCallbackGroup()

        self._ingestor_name = ingestor_name
        self.current_ingestor_mode = False

        # Publish into the animated actor
        self._animation_set_pub = self.create_publisher(
            String, "/animation_set", 1)

        self.ingestor_state_sub = self.create_subscription(
            IngestorState,
            '/ingestor_states',
            self.ingestor_state_cb,
            QoSProfile(depth=10, reliability=ReliabilityPolicy.RELIABLE),
            callback_group=self.group1)

        self.current_anim = "idle"
        self.timer_period = 0.2
        self.timer = self.create_timer(
            self.timer_period, self.timer_callback, callback_group=self.group3)

        self.get_logger().warning("INGESTOR ANIMATOR READY for "+str(self._ingestor_name))

    def set_animation(self, animation_name):

        anim_str = String()

        # To avoid publishing not supported animations
        if animation_name == "wave":
            anim_str.data = animation_name
        elif animation_name == "idle":
            anim_str.data = animation_name
        elif animation_name == "working":
            anim_str.data = animation_name
        else:
            anim_str.data = "idle"

        self.current_anim = animation_name
        self._animation_set_pub.publish(anim_str)
        self.get_logger().info("Animation "+str(self.current_anim)+" SET")

    def ingestor_state_cb(self, msg):
        """
        guid: table_ingestor_1
        mode: 0
        """
        self.current_ingestor_mode = msg.mode

        if msg.guid == self._ingestor_name:
            if self.current_ingestor_mode == 1:
                self.set_animation(animation_name="working")
            else:
                self.set_animation(animation_name="idle")
        else:
            # Info not for me
            pass

    def timer_callback(self):
        self.set_animation(animation_name=self.current_anim)


def main(args=None):

    rclpy.init(args=args)

    simple_publisher = AnimatedIngestor()

    # We have potentially 3 callback at the same time, 2 for sure
    num_threads = 2

    simple_publisher.get_logger().info('NUM threads='+str(num_threads))

    executor = MultiThreadedExecutor(num_threads=num_threads)
    executor.add_node(simple_publisher)

    try:
        executor.spin()
    finally:
        executor.shutdown()
        simple_publisher.destroy_node()

    rclpy.shutdown()


if __name__ == '__main__':
    main()
