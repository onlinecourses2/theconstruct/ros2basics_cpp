/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <functional>

#include "gazebo/physics/physics.hh"
#include <animated_actors/AnimatedDispenser.hpp>
#include <ignition/math.hh>

using namespace gazebo;
GZ_REGISTER_MODEL_PLUGIN(ActorPlugin);

#define WAVING_ANIMATION "waving"
#define IDLE_ANIMATION "idle"
#define WORKING_ANIMATION "working"

/////////////////////////////////////////////////
ActorPlugin::ActorPlugin() {}

/////////////////////////////////////////////////
void ActorPlugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf) {
  this->sdf = _sdf;
  this->actor = boost::dynamic_pointer_cast<physics::Actor>(_model);
  this->world = this->actor->GetWorld();

  this->connections.push_back(event::Events::ConnectWorldUpdateBegin(
      std::bind(&ActorPlugin::OnUpdate, this, std::placeholders::_1)));

  this->Reset();

  // Initialize ROS node
  this->ros_node_ = gazebo_ros::Node::Get(_sdf);

  // Get QoS profiles
  const gazebo_ros::QoS &qos = this->ros_node_->get_qos();

  // Read in the animation factor (applied in the OnUpdate function).
  if (_sdf->HasElement("animation_factor"))
    this->animationFactor = _sdf->Get<double>("animation_factor");
  else
    this->animationFactor = 4.5;

  this->animation_sub_ =
      this->ros_node_->create_subscription<std_msgs::msg::String>(
          "/animation_set",
          qos.get_subscription_qos("animation_set", rclcpp::QoS(1)),
          std::bind(&ActorPlugin::OnAnimationSet, this, std::placeholders::_1));

  RCLCPP_INFO(this->ros_node_->get_logger(), "Subscribed to [%s]",
              this->animation_sub_->get_topic_name());

  gzwarn << "LOADED ANIMATED DISPENSER PLUGIN.\n";
}

void ActorPlugin::OnAnimationSet(const std_msgs::msg::String::SharedPtr _msg) {
  std::lock_guard<std::mutex> scoped_lock(lock_);

  this->current_animation.data = _msg->data;

  // Create custom trajectory
  if (this->current_animation.data == "wave") {
    this->trajectoryInfo.reset(new physics::TrajectoryInfo());
    this->trajectoryInfo->type = WAVING_ANIMATION;
    this->trajectoryInfo->duration = 1.0;
  } else if (this->current_animation.data == "working") {
    this->trajectoryInfo.reset(new physics::TrajectoryInfo());
    this->trajectoryInfo->type = WORKING_ANIMATION;
    this->trajectoryInfo->duration = 1.0;
  } else if (this->current_animation.data == "idle") {
    this->trajectoryInfo.reset(new physics::TrajectoryInfo());
    this->trajectoryInfo->type = IDLE_ANIMATION;
    this->trajectoryInfo->duration = 1.0;
  } else {
    RCLCPP_ERROR(this->ros_node_->get_logger(),
                 "NOT SUPPORTED ANIMATION SET= [%s]",
                 this->current_animation.data.c_str());
  }

  this->actor->SetCustomTrajectory(this->trajectoryInfo);

  RCLCPP_INFO(this->ros_node_->get_logger(), "INPUT ANIMATION SET= [%s]",
              this->current_animation.data.c_str());
}

/////////////////////////////////////////////////
void ActorPlugin::Reset() {

  this->lastUpdate = 0;

  auto skelAnims = this->actor->SkeletonAnimations();
  if (skelAnims.find(IDLE_ANIMATION) == skelAnims.end()) {
    gzerr << "Skeleton animation " << IDLE_ANIMATION << " not found.\n";
  } else {
    // Create custom trajectory
    this->trajectoryInfo.reset(new physics::TrajectoryInfo());
    this->trajectoryInfo->type = IDLE_ANIMATION;
    this->trajectoryInfo->duration = 1.0;

    this->actor->SetCustomTrajectory(this->trajectoryInfo);
  }
}

/////////////////////////////////////////////////
void ActorPlugin::OnUpdate(const common::UpdateInfo &_info) {
  // Time delta
  double dt = (_info.simTime - this->lastUpdate).Double();

  // animation
  this->actor->SetScriptTime(this->actor->ScriptTime() + dt);
  this->lastUpdate = _info.simTime;
}