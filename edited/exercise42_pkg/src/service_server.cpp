#include "geometry_msgs/msg/twist.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_srvs/srv/set_bool.hpp"

#include <memory>

using StdBool = std_srvs::srv::SetBool;
using std::placeholders::_1;
using std::placeholders::_2;

class ServiceServerNode : public rclcpp::Node {
public:
  ServiceServerNode() : Node("service_moving") {

    srv_ = create_service<StdBool>(
        "turn_right",
        std::bind(&ServiceServerNode::moving_callback, this, _1, _2));
    publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
  }

private:
  rclcpp::Service<StdBool>::SharedPtr srv_;
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;

  void moving_callback(const std::shared_ptr<StdBool::Request> request,
                       const std::shared_ptr<StdBool::Response> response) {

    auto message = geometry_msgs::msg::Twist();

    if (request->data) {
      message.linear.x = 0.2;
      message.angular.z = -0.2;
      response->success = true;
      response->message = "Turning to the right right right!";
    } else {
      message.linear.x = 0.0;
      message.angular.z = 0.0;
      // Set the response success variable to false
      response->success = false;
      // Set the response message variable to a string
      response->message = "It is time to stop!";
    }
    publisher_->publish(message);
  }
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<ServiceServerNode>());
  rclcpp::shutdown();
  return 0;
}