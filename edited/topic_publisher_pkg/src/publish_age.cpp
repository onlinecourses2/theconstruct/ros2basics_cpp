// Exercise
// Inside this topic_publisher_pkg package, create a new C++ file named
// publish_age.cpp Inside this new C++ script, create a Publisher node that
// indicates the age of the robot. For this, use the new interface Age.msg
// created in Section 3.5.

#include "custom_interfaces/msg/age.hpp"
#include "rclcpp/rclcpp.hpp"
#include <chrono>
#include <cstdio>

using namespace std::chrono_literals;

class AgePublisher : public rclcpp::Node {
public:
  AgePublisher() : Node("age_publisher_node"){
    publisher_ =
        this->create_publisher<custom_interfaces::msg::Age>("/age", 10);
    timer_ = this->create_wall_timer(
        500ms, std::bind(&AgePublisher::timer_callback, this));
  }

private:
  void timer_callback() {
    auto message = custom_interfaces::msg::Age();
    message.years = 1.0;
    message.months = 5.0;
    message.days = 11.0;
    publisher_->publish(message);
  }
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<custom_interfaces::msg::Age>::SharedPtr publisher_;
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<AgePublisher>());
  rclcpp::shutdown();
  return 0;
}