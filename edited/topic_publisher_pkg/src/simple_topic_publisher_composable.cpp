// Exercise
// Inside the topic_publisher_pkg, create a new C++ script named move_robot.cpp.
// Inside the new script, create a Publisher node that publishes data to the
// /cmd_vel Topic to move the robot in circles. Create a launch file named
// move_robot.launch.py that starts your new Publisher node. Compile your
// package again. Launch the program and check that the robot moves correctly.

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/int32.hpp"
#include <chrono>

using namespace std::chrono_literals;

/* This example creates a subclass of Node and uses std::bind() to register a
 * member function as a callback from the timer. */
// First, define your class, which inherits from the rclcpp::Node class.

// Next, have the constructor of your class

// Within the constructor, initialize your node by calling the constructor of
// the superclass node, and also initialize a variable named count_ to 0.

class SimplePublisher : public rclcpp::Node {
public:
  SimplePublisher() : Node("simple_publisher"), count_(0) {
    publisher_ = this->create_publisher<std_msgs::msg::Int32>("counter", 10);
    timer_ = this->create_wall_timer(
        500ms, std::bind(&SimplePublisher::timer_callback, this));
  }

private:
  void timer_callback() {
    auto message = std_msgs::msg::Int32();
    message.data = count_;
    count_++;
    publisher_->publish(message);
  }
  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<std_msgs::msg::Int32>::SharedPtr publisher_;
  size_t count_;
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<SimplePublisher>());
  rclcpp::shutdown();
  return 0;
}