// Project
// Create a code to make the robot avoid the sphere in front of him

#include "geometry_msgs/msg/twist.hpp"
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/detail/laser_scan__struct.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include <chrono>

using namespace std::chrono_literals;
using std::placeholders::_1;

class SimpleObstacleAvoid : public rclcpp::Node {
public:
  SimpleObstacleAvoid() : Node("topics_quiz_node") {
    cmd_vel_publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("/cmd_vel", 10);
    scan_sub_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
        "/scan", 10, std::bind(&SimpleObstacleAvoid::scanCB, this, _1));
    final_vel_.linear.x = 0.0;
    final_vel_.angular.z = 0.0;
  }

private:
  void setVelocity(double lx, double az) {
    final_vel_.linear.x = lx;
    final_vel_.angular.z = az;
  }

  float smallestOfArray(float arr[], int n) {
    float temp = 2.0;
    for (int i = 0; i < n; i++) {
      if (temp > arr[i]) {
        temp = arr[i];
      }
    }
    return temp;
  }

  void getObstacleArea(float right, float center, float left) {
    if (right > 1 and center > 1 and left > 1) {
      status = "NO OBSTACLE!";
      setVelocity(0.6, 0.0);
    } else if (right > 1 and center < 1 and left > 1) {
      status = "OBSTACLE CENTER!";
      setVelocity(0.0, -0.3);
    } else if (right < 1 and center > 1 and left > 1) {
      status = "OBSTACLE RIGHT!";
      setVelocity(0.0, -0.3);
    } else if (right > 1 and center > 1 and left < 1) {
      status = "OBSTACLE LEFT!";
      setVelocity(0.0, 0.3);
    } else if (right < 1 and center > 1 and left < 1) {
      status = "OBSTACLE RIGHT AND LEFT!";
      setVelocity(0.0, -0.3);
    } else if (right > 1 and center < 1 and left < 1) {
      status = "OBSTACLE CENTER AND LEFT!";
      setVelocity(0.0, 0.3);
    } else if (right < 1 and center < 1 and left > 1) {
      status = "OBSTACLE CENTER AND RIGHT!";
      setVelocity(0.0, -0.3);
    } else if (right < 1 and center < 1 and left < 1) {
      status = "OBSTACLE AHEAD!";
      setVelocity(0.0, -0.3);
    }
  }

  void scanCB(const sensor_msgs::msg::LaserScan::SharedPtr msg) {

    int length = msg->ranges.size(); // it is 720
    int lendiv3 = length / 3;        // length division is 240
    float right[length / 3], center[length / 3], left[length / 3];
    for (int i = 0; i < length; i++) {
      if (i < lendiv3) {
        right[i] = msg->ranges[i];
      } else if ((i > lendiv3 - 1) && (i < 2 * lendiv3)) {
        center[i - 240] = msg->ranges[i];
      } else if (((2 * lendiv3) - 1) && (i < length)) {
        left[i - 480] = msg->ranges[i];
      }
    }
    float rightmin = smallestOfArray(right, length / 3);
    float centermin = smallestOfArray(center, length / 3);
    float leftmin = smallestOfArray(left, length / 3);

    getObstacleArea(rightmin, centermin, leftmin);
    std::cout << status << std::endl;
    cmd_vel_publisher_->publish(final_vel_);
  }

  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr cmd_vel_publisher_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr scan_sub_;
  geometry_msgs::msg::Twist final_vel_;
  std::string status = "";
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<SimpleObstacleAvoid>());
  rclcpp::shutdown();
  return 0;
}